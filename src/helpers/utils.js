
function play_int(game, workspace) {
    $('#play').val("pause");
    // $('#play').text("Сброс");
    $('#play').find('span').text("Сбросить");
    runProgram(game, workspace);
    // do play
}

function play_pause(game) {
    $('#play').val("play");
    $('#play').find('span').text("Запуск");
    resetProgram(game);
    //$('#play').text("Запуск");
    // do pause
}


function runProgram(game, workspace) {

    try {
        eval(Blockly.JavaScript.workspaceToCode(workspace));
    } catch (e) {
        console.log(e);
    }
    game.scene.keys['GameScene'].player.execute();
}


function resetProgram(game) {
    const thisGame = game.scene.keys['GameScene'];
    thisGame.player.reset();
}



function toogleRunButton(game, workspace) {
    $('#play').val() == "play" ? play_int(game, workspace) : play_pause(game);
    $('#play').find('i:first').toggleClass('fa-play fa-refresh');
    $('#play').toggleClass('play reset');
}

export { play_int, play_pause, toogleRunButton }