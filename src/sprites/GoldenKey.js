

export default class GoldenKey extends Phaser.Physics.Arcade.Sprite {

    constructor(scene, x, y, sprite) {
        super(scene, x, y, sprite);
        scene.add.existing(this);
        scene.physics.world.enable(this);


        this.setPosition(x, y);
        this.play('pickdown');
        this.overlapTriggered = false;
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);
    }

}
