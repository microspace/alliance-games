import {
    config,
    dir2vec
} from '../helpers/config'


export default class Player extends Phaser.Physics.Arcade.Sprite {

    constructor(scene, x, y, sprite) {
        super(scene, x, y, sprite);
        scene.add.existing(this);
        scene.physics.world.enable(this);
        this.body.setSize(50, 13);
        this.body.setOffset(40, 73);

        this.setPosition(x, y);
        this.play('idle');
        this.stack = [];

        this.pos = {
            x: Math.floor(x / config.tileSize),
            y: Math.floor(y / config.tileSize)
        };

        this.lastPos = {
            x: x,
            y: y
        };
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);
        this.scene.physics.world.collide(this, this.scene.blockLayer);
        this.scene.physics.world.collide(this, this.scene.sinkLayer);
        this.scene.physics.world.collide(this, this.scene.flour);

    }

    fireBullet() {
        let bullet = this.scene.bullets.get();
        if (bullet) {
            bullet.fire(this.x, this.y, this.flipX);
            this.play('fire');
            this.on('animationcomplete', () => {
                this.play('idle');
            }, this);
            this.scene.events.on('bullethit', () => {
                this.executeNext()
            }, this);
        }
    }

    move(vector) {
        if (vector.x < 0) {
            this.flipX = true;
        } else if (vector.x > 0) {
            this.flipX = false;
        }
        this.vector = vector;
        this.play('walk');
        this.scene.tweens.add({
            targets: this,
            x: this.x + config.tileSize * vector.x,
            y: this.y + config.tileSize * vector.y,
            duration: config.stepTime * Math.max(Math.abs(vector.x), Math.abs(vector.y)),
            ease: 'Linear',
            onComplete: () => {
                this.play('idle')
                this.executeNext();
            },
        });

    }

    uturn() {
        this.flipX = !this.flipX;
        this.executeNext();
    }

    reset() {
        // очищаем стэк команд
        this.stack = [];
        this.play("idle");
        this.scene.tweens.killAll();

        this.flipX = false;
        this.scaleX = 1;
        this.scaleY = 1;
        this.visible = true;
        this.hasGoldenKey = false;
        this.result = this.scene.parseObjectLayers('goldenKey', this.scene.map, 'objectLayer');
        this.scene.goldenKey.x = this.result.x;
        this.scene.goldenKey.y = this.result.y;

        // устанавливаем в исходную точку (в пикселях)
        this.setPosition(this.lastPos.x, this.lastPos.y);

        if (this.scene === 2) {
            this.scene.destroyBridge();
        }


        // задержка чтобы игрок успел вернуться в начальную точку и за это время чтобы не сработал колбэк
        this.scene.time.addEvent({
            delay: 500,
            callback: () => {
                this.scene.hitFlag = false;
                this.scene.sinkFlag = false;
            },
            loop: false
        })

    }

    schedule(arg) {
        this.stack.push(arg);
    }

    execute() {
        //  todo:  убрать время если камера близко к игроку
        this.scene.cameras.main.pan(this.x, this.y, 50, 'Linear');


        //  наводим камеру, затем запускаем код
        /*         this.scene.cameras.main.on(Phaser.Cameras.Scene2D.Events.PAN_COMPLETE, () => {
         
                }); */

        this.scene.cameras.main.startFollow(this, true, 0.08, 0.08);
        this.executeNext();
    }

    executeNext() {
        if (this.stack.length <= 0) {
            this.scene.checkFinal();
            return;
        }

        let action = this.stack.shift();

        switch (action.name) {
            case "move":
                this.move(action.vector);
                break;
            case "uturn":
                this.uturn();
                break;
            case "fire":
                this.fireBullet();
                break;

        }
    }



}
