/*
import AnimatedTiles from 'phaser-animated-tiles/dist/AnimatedTiles.min.js';
*/
import Player from '../sprites/Player'
import Pointer from '../sprites/Pointer'
import Bullet from '../sprites/Bullet'
import GoldenKey from '../sprites/GoldenKey'

import { toogleRunButton } from '../helpers/utils';

class GameScene extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'GameScene'
        });
    }

    preload() {
        // this.load.scenePlugin('animatedTiles', AnimatedTiles, 'animatedTiles', 'animatedTiles');
    }

    getCoord(subscene) {
        let playerXY, pointerXY;
        if (subscene == 0) {
            playerXY = this.parseObjectLayers('playerStartPosition', this.map, 'playerLayer');
            pointerXY = this.parseObjectLayers('scene1Goal', this.map, 'playerLayer');
        } else if (subscene == 1) {
            playerXY = this.parseObjectLayers('scene1Goal', this.map, 'playerLayer');
            pointerXY = this.parseObjectLayers('scene2Goal', this.map, 'playerLayer');
        } else if (subscene == 2) {
            playerXY = this.parseObjectLayers('scene2Goal', this.map, 'playerLayer');
            pointerXY = this.parseObjectLayers('scene3Goal', this.map, 'playerLayer');
        } else if (subscene == 3) {
            playerXY = this.parseObjectLayers('scene3Goal', this.map, 'playerLayer');
            pointerXY = this.parseObjectLayers('scene4Goal', this.map, 'playerLayer');
        } else if (subscene == 4) {
            playerXY = this.parseObjectLayers('scene4Goal', this.map, 'playerLayer');
            pointerXY = this.parseObjectLayers('scene5Goal', this.map, 'playerLayer');
        }
        return {
            playerXY: playerXY,
            pointerXY: pointerXY
        }
    }
    create() {
        this.subscene = 0;
        this.hitFlag = false;
        this.sinkFlag = false;

        // Add the map + bind the tileset
        this.map = this.make.tilemap({
            key: 'map'
        });
        this.tileset = this.map.addTilesetImage('tileSheet04-01', 'tiles');

        this.playerXY = this.getCoord(this.subscene).playerXY;
        this.pointerXY = this.getCoord(this.subscene).pointerXY;


        // Dynamic layer because we want breakable and animated tiles
        // this.groundLayer = this.map.createDynamicLayer('world', this.tileset, 0, 0);

        this.flour = this.map.createDynamicLayer("flour", this.tileset, 0, 0);
        this.onFlour = this.map.createStaticLayer("onFlour", this.tileset, 0, 0);
        this.blockLayer = this.map.createDynamicLayer("blockLayer", this.tileset, 0, 0);
        this.sinkLayer = this.map.createDynamicLayer("sinkLayer", this.tileset, 0, 0);
        this.onBlockLayer = this.map.createStaticLayer("onBlockLayer", this.tileset, 0, 0);

        this.player = new Player(this, this.playerXY.x, this.playerXY.y, 'player');
        this.pointer = new Pointer(this, this.pointerXY.x, this.pointerXY.y, 'pointer');
        this.result = this.parseObjectLayers('goldenKey', this.map, 'objectLayer');
        this.goldenKey = new GoldenKey(this, this.result.x + 16, this.result.y - 16, "goldenKey")

        this.upperLayer = this.map.createStaticLayer("upperLayer", this.tileset, 0, 0);

        // В Тайлед для тайла нужно добавить такое свойство для столкновений
        /*         this.blockLayer.setCollisionByProperty({
                    collides: true
                }); */

        /*         const debugGraphics = this.add.graphics().setAlpha(0.75);
                this.blockLayer.renderDebug(debugGraphics, {
                    tileColor: null, // Color of non-colliding tiles
                    collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
                    faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
                }); */

        //this.physics.add.collider(this.player, this.blockLayer);
        this.map.setTileIndexCallback([15, 16, 17, 18, 28, 29, 30, 31, 32, 35, 36, 37, 169, 231, 209, 210, 222, 223], this.hitWall, this, this.blockLayer);
        this.map.setTileIndexCallback([83, 129, 130, 131, 132, 133, 134], this.sinkInWater, this, this.sinkLayer);
        this.map.setTileIndexCallback(229, this.restoreBridge, this, this.flour);
        this.physics.add.overlap(this.player, this.goldenKey, this.goldenKeyCallback, null, this);

        this.keys = {

            left: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT),
            right: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT),
            down: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN),
            up: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP),
            zoomIn: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q),
            zoomOut: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E),
        };


        // ограничиваем движения камеры под размер карты

        this.cursors = this.input.keyboard.createCursorKeys();
        var controlConfig = {
            camera: this.cameras.main,
            left: this.cursors.left,
            right: this.cursors.right,
            up: this.cursors.up,
            down: this.cursors.down,
            speed: 0.5,
            disableCull: true,

        };
        this.controls = new Phaser.Cameras.Controls.FixedKeyControl(controlConfig);

        this.particles = this.add.particles('explosion');

        this.particles.createEmitter({
            frame: 'muzzleflash2',
            lifespan: 200,
            scale: {
                start: .5,
                end: 0
            },
            rotate: {
                start: 0,
                end: 180
            },
            on: false
        });

        this.bullets = this.add.group({
            classType: Bullet,
            maxSize: 10,
            runChildUpdate: true
        });


        this.scale.on('resize', () => {
            //  подгоняем карту под размер канваса
            const canvasH = document.getElementById("phaser-example").offsetHeight;
            const canvasW = document.getElementById("phaser-example").offsetWidth;
            this.cameras.main.setSize(canvasW, canvasH);
        });




        // this.player.setDepth(1); // Игрок поверх пуль

        var camera = this.cameras.main;
        var dragScale = this.plugins.get('rexpinchplugin').add(this);

        dragScale
            .on('drag1', function (dragScale) {
                var drag1Vector = dragScale.drag1Vector;
                camera.stopFollow();
                camera.scrollX -= drag1Vector.x / camera.zoom;
                camera.scrollY -= drag1Vector.y / camera.zoom;
            })
            .on('pinch', function (dragScale) {
                var scaleFactor = dragScale.scaleFactor;
                if (camera.displayHeight <= this.map.heightInPixels) {
                    camera.zoom *= scaleFactor;
                }
                
            }, this)

        camera.zoom = 0.5;
        camera.startFollow(this.player, true, 0.08, 0.08);
        camera.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        camera.roundPixels = true;
        this.showModal("Привет! Ты должен дойти до пункта сбора. Управляй робокостюмом с помощью блоков и добирайся до указателей. Используй жесты для навигации по карте", 0);

        this.destroyBridge();

    }

    update(time, delta) {
        this.controls.update(delta);
        if (this.keys.up.isDown || this.keys.down.isDown || this.keys.left.isDown || this.keys.right.isDown) {
            this.cameras.main.stopFollow();
        }
        if (this.keys.zoomIn.isDown) {
            this.cameras.main.setZoom(this.cameras.main.zoom + 0.01);
        } else if (this.keys.zoomOut.isDown) {
            if (this.cameras.main.displayHeight <= this.map.heightInPixels) {
                this.cameras.main.setZoom(this.cameras.main.zoom - 0.01);
            }
        }
        if (this.player.hasGoldenKey) {
            this.goldenKey.x = this.player.x - 40;
            this.goldenKey.y = this.player.y - 40;
        }
    }
    goldenKeyCallback() {
        if (!this.keyOverlapFlag) {
            this.goldenKey.play("pickup");
            this.player.hasGoldenKey = true;
        }
    }
    destroyBridge() {
        this.map.removeTileAt(19, 7, true, true, this.flour);
        this.map.removeTileAt(20, 7, true, true, this.flour);
        this.map.removeTileAt(21, 7, true, true, this.flour);
        this.map.removeTileAt(19, 8, true, true, this.flour);
        this.map.removeTileAt(20, 8, true, true, this.flour);
        this.map.removeTileAt(21, 8, true, true, this.flour);
        this.map.removeTileAt(19, 9, true, true, this.flour);
        this.map.removeTileAt(20, 9, true, true, this.flour);
        this.map.removeTileAt(21, 9, true, true, this.flour);

        this.map.putTileAt(145, 19, 7, true, this.sinkLayer);
        this.map.putTileAt(145, 20, 7, true, this.sinkLayer);
        this.map.putTileAt(145, 21, 7, true, this.sinkLayer);
        this.map.putTileAt(134, 19, 8, true, this.sinkLayer);
        this.map.putTileAt(134, 20, 8, true, this.sinkLayer);
        this.map.putTileAt(134, 21, 8, true, this.sinkLayer);
        this.map.putTileAt(134, 19, 9, true, this.sinkLayer);
        this.map.putTileAt(134, 20, 9, true, this.sinkLayer);
        this.map.putTileAt(134, 21, 9, true, this.sinkLayer);

        this.map.replaceByIndex(228, 229, 21, 2, 1, 1, this.flour);
    }

    restoreBridge() {
        this.map.removeTileAt(19, 7, true, true, this.sinkLayer);
        this.map.removeTileAt(20, 7, true, true, this.sinkLayer);
        this.map.removeTileAt(21, 7, true, true, this.sinkLayer);
        this.map.removeTileAt(19, 8, true, true, this.sinkLayer);
        this.map.removeTileAt(20, 8, true, true, this.sinkLayer);
        this.map.removeTileAt(21, 8, true, true, this.sinkLayer);
        this.map.removeTileAt(19, 9, true, true, this.sinkLayer);
        this.map.removeTileAt(20, 9, true, true, this.sinkLayer);
        this.map.removeTileAt(21, 9, true, true, this.sinkLayer);

        this.map.putTileAt(107, 19, 7, true, this.flour);
        this.map.putTileAt(107, 20, 7, true, this.flour);
        this.map.putTileAt(107, 21, 7, true, this.flour);
        this.map.putTileAt(107, 19, 8, true, this.flour);
        this.map.putTileAt(107, 20, 8, true, this.flour);
        this.map.putTileAt(107, 21, 8, true, this.flour);
        this.map.putTileAt(107, 19, 9, true, this.flour);
        this.map.putTileAt(107, 20, 9, true, this.flour);
        this.map.putTileAt(107, 21, 9, true, this.flour);

        this.map.replaceByIndex(229, 228, 21, 2, 1, 1, this.flour);
    }
    hitWall() {
        if (!this.hitFlag) {
            /*             if (this.player.vector.y < 0) {
                            this.player.y += 5
                        } else if (this.player.vector.y > 0) {
                            this.player.y -= 5
                        }
                        if (this.player.vector.x < 0) {
                            this.player.x += 5
                        } else if (this.player.vector.x > 0) {
                            this.player.x -= 5
                        } */
            this.player.stack = [];
            this.player.play("hitted");
            this.tweens.killAll();
            this.hitFlag = true;
            console.log("я столнулся со стеной")
        }
    }

    sinkInWater() {
        if (!this.sinkFlag) {
            this.sinkFlag = true;
            this.player.stack = [];
            this.tweens.killAll();
            this.tweens.add({
                targets: this.player,
                x: this.player.x + Math.sign(this.player.vector.x) * 64,
                y: this.player.y + Math.sign(this.player.vector.y) * 64,
                duration: 1000,
                ease: 'Linear',
                onComplete: () => {
                    this.tweens.add({
                        targets: this.player,
                        scaleX: 0.1,
                        scaleY: 0.1,
                        duration: 500,
                        ease: 'Linear',
                        onComplete: () => {
                            this.player.visible = false;
                            console.log("я утонул")
                        },
                    });
                },
            });
        }
    }
    showModal(text, mood) {
        document.getElementById("modaltext").innerHTML = text;
        document.getElementById("modal").style.display = "block";

        if (mood === 2) {
            document.getElementById("imagecontainer").classList.remove('hero_fail');
            document.getElementById("imagecontainer").classList.remove('hero');
            document.getElementById("imagecontainer").classList.add('hero_win');
        } else if (mood === 1) {
            document.getElementById("imagecontainer").classList.remove('hero');
            document.getElementById("imagecontainer").classList.remove('hero_win');
            document.getElementById("imagecontainer").classList.add('hero_fail');
        } else {
            document.getElementById("imagecontainer").classList.remove('hero_fail');
            document.getElementById("imagecontainer").classList.remove('hero_win');
            document.getElementById("imagecontainer").classList.add('hero');
        }

    }

    parseObjectLayers(type, map, layer) {
        let t = map.objects.filter(v => v.name == layer);
        let s = t[0].objects.filter(v => v.name == type);
        return {
            x: s[0].x,
            y: s[0].y
        };
    }

    checkFinal() {
        let isOverlapping = this.physics.world.overlap(this.player, this.pointer, null, null, this);
        if (isOverlapping) {
            if (this.subscene === 0) {
                this.showModal("Отлично получилось! Теперь попробуй дойди до следующего указателя!", 0)

                this.subscene = 1;


                this.pointerXY = this.getCoord(this.subscene).pointerXY;
                // перемещаем указатель в новую точку
                this.pointer.x = this.pointerXY.x;
                this.pointer.y = this.pointerXY.y;
                // записываем 
                this.player.lastPos.x = this.player.x;
                this.player.lastPos.y = this.player.y;


                toogleRunButton(this.game);
                try {
                    setIsCheckedForLesson();
                } catch (e) {
                    console.log("couldn't set IsChecked For Lesson");
                }


            } else if (this.subscene === 1) {
                const messagetext = "Превосходно! Нажми на кнопку, чтобы восстановить мост!";
                this.showModal(messagetext, 0)
                this.subscene = 2;
                // перемещаем указатель в новую точку
                this.pointerXY = this.getCoord(this.subscene).pointerXY;
                this.pointer.x = this.pointerXY.x;
                this.pointer.y = this.pointerXY.y;
                // записываем 
                this.player.lastPos.x = this.player.x;
                this.player.lastPos.y = this.player.y;

                toogleRunButton(this.game);

            } else if (this.subscene === 2) {
                var messagetext = "Теперь нужно дойти до больших ворот, но они закрыты. Чтобы их открыть нужно взять ключ!";
                this.showModal(messagetext, 0)

                this.subscene = 3;
                // перемещаем указатель в новую точку
                this.pointerXY = this.getCoord(this.subscene).pointerXY;
                this.pointer.x = this.pointerXY.x;
                this.pointer.y = this.pointerXY.y;
                // записываем 
                this.player.lastPos.x = this.player.x;
                this.player.lastPos.y = this.player.y;

                toogleRunButton(this.game);

            } else if (this.subscene === 3) {


                if (this.player.hasGoldenKey) {
                    this.subscene = 4;
     
                    this.time.addEvent({
                        delay: 500,
                        callback: () => {
                            this.map.replaceByIndex(196, 198, 31, 8, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(197, 199, 32, 8, 1, 1, this.blockLayer);
    
                            this.map.replaceByIndex(209, 211, 31, 9, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(210, 212, 32, 9, 1, 1, this.blockLayer);
    
                            this.map.replaceByIndex(222, 224, 31, 10, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(223, 225, 32, 10, 1, 1, this.blockLayer);
                        },
                        loop: false
                    });
                    this.time.addEvent({
                        delay: 1000,
                        callback: () => {
                            this.map.replaceByIndex(198, 200, 31, 8, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(199, 201, 32, 8, 1, 1, this.blockLayer);
    
                            this.map.replaceByIndex(211, 213, 31, 9, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(212, 214, 32, 9, 1, 1, this.blockLayer);
    
                            this.map.replaceByIndex(224, 226, 31, 10, 1, 1, this.blockLayer);
                            this.map.replaceByIndex(225, 227, 32, 10, 1, 1, this.blockLayer);
                        },
                        loop: false
                    });

                    this.pointerXY = this.getCoord(this.subscene).pointerXY;
                    this.pointer.x = this.pointerXY.x;
                    this.pointer.y = this.pointerXY.y;
                    // записываем 
                    this.player.lastPos.x = this.player.x;
                    this.player.lastPos.y = this.player.y;
    
                    toogleRunButton(this.game);
                }
            } else if (this.subscene === 4) {
                var messagetext = "Отлично! Все в сборе! Теперь мы летим в космос! Там вас ждет учебный бой против наших роботов.";
                this.showModal(messagetext, 2)

                this.player.lastPos.x = this.player.x;
                this.player.lastPos.y = this.player.y;
                this.pointer.destroy();

            }
        }
    }


}

export default GameScene;
