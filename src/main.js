import 'phaser';

import BootScene from './scenes/BootScene';
import GameScene from './scenes/GameScene';
import { toogleRunButton } from './helpers/utils';


require('./blocks')
let options = {
    comments: true,
    collapse: true,
    disable: true,
    maxBlocks: Infinity,
    oneBasedIndex: true,
    readOnly: false,
    scrollbars: true,
    trashcan: true,
    toolbox: document.getElementById('toolbox'),
    horizontalLayout: false,
    toolboxPosition: 'start',
    zoom: {
        controsls: false,
        wheel: true,
        startScale: 0.6,
        maxScale: 4,
        minScale: 0.25,
        scaleSpeed: 1.1
    }
};

let workspace = Blockly.inject('bdd', options);

Blockly.BlockSvg.START_HAT = true;
Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'), workspace);
workspace.addChangeListener(Blockly.Events.disableOrphans);

function onHideHat(event) {
    if (event.type == Blockly.Events.MOVE) {
        try {
            let bl = workspace.getBlockById(event.blockId);
            var dx = Math.abs(event.newCoordinate.x - event.oldCoordinate.x);
            if (event.newCoordinate.x < 0 && bl.type == "factory_base") {
                bl.moveBy(dx, 0);
            }
        } catch (e) {
        }
    }
}
workspace.addChangeListener(onHideHat);

//simport tingle from 'tingle'
const config = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.RESIZE,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: '100%',
        height: '100%'
    },
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    render: {
        pixelArt: false,
        antialias: true,
        roundPixels: true,
    },
    pixelArt: false,
    antialias: true,
    roundPixels: true,
    backgroundColor: '#2d2d2d',
    parent: 'phaser-example',
    scene: [
        BootScene,
        GameScene
    ]
};

const game = new Phaser.Game(config);



let el = document.getElementById("play");
el.addEventListener("click", function () {
    toogleRunButton(game, workspace)
}, false)


